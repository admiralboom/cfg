This repo contains the Bash environment, including scripts, aliases and
functions for my linux configuration.

This repo gets pulled from the base_config ansible role:
https://gitlab.com/admiralboom/base_config/-/blob/master/tasks/main.yml#L87

A cron updates the local http read-only clone:
https://gitlab.com/admiralboom/base_config/-/blob/master/tasks/main.yml#L162

base_config role gets included into build configs, such as Workstation:
https://gitlab.com/admiralboom/workstation/-/blob/master/main.yaml#L10
