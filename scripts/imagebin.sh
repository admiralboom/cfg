#!/bin/bash

## Defaults
WANT_PROGRESS_BARS=1

## Output type
## Change this to "html" for the same HTML5 output you'd get in a browser
output_type="api"

## Where to send the pictures
target_uri='http://imagebin.domain.com/upload.php'

###############################################################################
###############################################################################

output_separator=' '

## Process command-line options
while getopts 'd:p' OPT; do
  case ${OPT} in
    'd') output_separator="${OPTARG}" ; shift 2 ;;
    'p') WANT_PROGRESS_BARS=0         ; shift 1 ;;
  esac
done

HAVE_PV=0       ; which pv       >/dev/null 2>&1 && HAVE_PV=1
HAVE_BASENAME=0 ; which basename >/dev/null 2>&1 && HAVE_BASENAME=1

image_files="${@}"

for image_file in ${image_files}; do
  if [[ "${image_file}" =~ ^(ftp|https?):// ]]; then
    local_file="/tmp/imagebin-tempfile"
    curl -s -o "${local_file}" "${image_file}"
    mime_type=$(file --brief --mime-type "${local_file}")
    case "${mime_type}" in
      video/3gpp)      new_extension='3gp' ;;
      image/jpeg)      new_extension='jpg' ;;
      application/pdf) new_extension='pdf' ;;
      image/png)       new_extension='png' ;;
    esac
    mv "${local_file}" "${local_file}.${new_extension}" ; local_file="${local_file}.${new_extension}"
  else
    local_file="${image_file}"
  fi
  echo "Uploading ${local_file}" >&2
  if [ ${HAVE_PV} = 1 ] && [ ${HAVE_BASENAME} = 1 ] && [ ${WANT_PROGRESS_BARS} = 1 ]; then
    local_file_basename=`basename ${local_file}`
    public_uri=`pv "${local_file}" | curl -s -F "output_type=api" -F "image_1=@-;filename=${local_file_basename}" "${target_uri}"`
  else
    public_uri=`curl -s -F "output_type=${output_type}" -F "image_1=@${local_file}" "${target_uri}"`
  fi
  if [ -t 1 ]; then
    echo "${local_file}${output_separator}${public_uri}" | tr -d '\n'
    echo ""
  else
    echo "${local_file}${output_separator}${public_uri}" | tr -d '\n' | xmessage -center -buttons 'Copy destination URI to cut buffer:0,Cancel:1' -file -
    case ${?} in
      0) tr -d '\n' <<< "${public_uri}" | xclip -in ; break ;;
      1) break ;;
    esac
  fi
done

## EOF
########
