set nocompatible              " be iMproved, required
filetype on                  " required
set splitbelow
set splitright
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" Plugins will be downloaded under the specified directory.
call plug#begin('~/.vim/plugged')

" Declare the list of plugins.
" Git wrapper
 Plug 'tpope/vim-fugitive'
" maintainer wanted for ansible-yaml - commenting
" Plug 'chase/vim-ansible-yaml'
"
" auto close quotes brackets etc
 Plug 'Raimondi/delimitMate'
" rails enhancement
" Plug 'tpope/vim-rails'
" dev version of vims markdown, not requred
" Plug 'tpope/vim-markdown'
" https://github.com/greyblake/vim-preview <Leader>P open preview page
 Plug 'greyblake/vim-preview'
" vim dev version of ruby acceptance testing
" Plug 'tpope/vim-cucumber'

" tpopes config for basics
 Plug 'tpope/vim-sensible'

" low contrast color scheme
 Plug 'junegunn/seoul256.vim'

 " <leader>t file system tree view
 Plug 'scrooloose/nerdtree'

" Fonts all the things
" Plug 'vim-airline/vim-airline'
" Plug 'vim-airline/vim-airline-themes'

" Highlight tralining whitespace
 Plug 'ntpeters/vim-better-whitespace'

" json syntax highlighting https://github.com/leshill/vim-json
 Plug 'leshill/vim-json'
 Plug 'taxilian/vim-web-indent'
 Plug 'Yggdroot/indentLine'
 let g:indentLine_enabled = 1
 let g:indentLine_char_list = ['|', '¦', '┆', '┊']

" fast command prediction
"" Plug 'ms-jpq/coq_nvim', {'branch': 'coq'}
" 9000+ Snippets
"" Plug 'ms-jpq/coq.artifacts', {'branch': 'artifacts'}

 " ms-jpq/chadtree for consideration "better than nerdtree"

" HTML
"  Plug 'HTML-AutoCloseTag' " commented due to errors
  Plug 'hail2u/vim-css3-syntax'
  Plug 'juvenn/mustache.vim'

" signify shows the diffs on the left
if has('nvim') || has('patch-8.0.902')
  Plug 'mhinz/vim-signify'
else
  Plug 'mhinz/vim-signify', { 'branch': 'legacy' }
endif

" List ends here. Plugins become visible to Vim after this call.
call plug#end()

" Plugin 'tpope/vim-fugitive'

" My plugins:
""  Plugin 'YankRing.vim'
""    let g:yankring_history_dir = $HOME.'/.vim/'
""    let g:yankring_history_file = '.yankring_history'
""  Plugin 'mbbill/undotree'
""  Plugin 'rodjek/vim-puppet'
"
" depolete is deprecated
""  Plugin 'Shougo/deoplete.nvim'
""    let g:deoplete#enable_at_startup = 1
""    let g:deoplete#auto_complete_delay=20
""    let g:deoplete#enable_ignore_case=1
""    let g:deoplete#max_list=10000
""    let g:deoplete#max_menu_width=0
""  Plugin 'roxma/nvim-yarp'
""  Plugin 'roxma/vim-hug-neovim-rpc'
  " on upgrade of deoplete or neovim do:
  " pip3 install --upgrade pynvim (and/or neovim)
""  Plugin 'scrooloose/snipmate-snippets'
"
""  Plugin 'honza/vim-snippets'
"   Plugin 'Indent-Guides'
"  Plugin 'nathanaelkane/vim-indent-guides'
""  Plugin 'hynek/vim-python-pep8-indent'
""    let g:syntastic_python_flake8_args='--ignore=E501'

" Javascript
""  Plugin 'leshill/vim-json'
""  Plugin 'groenewege/vim-less'
""  Plugin 'taxilian/vim-web-indent'


" Ruby
""  Plugin 'markcornick/vim-vagrant'
""  Plugin 'ekalinin/Dockerfile.vim'
""  Plugin 'zhaocai/GoldenView.Vim'
""  let g:goldenview__enable_default_mapping=0

" colo slate
colo seoul256
set nocompatible
set encoding=utf-8
set binary

" presentation settings
set number              " precede each line with its line number
set relativenumber      " show relative number
set numberwidth=3       " number of culumns for line numbers
set textwidth=0         " Do not wrap words (insert)
set nowrap              " Do not wrap words (view)
set showcmd             " Show (partial) command in status line.
set showmatch           " Show matching brackets.
set ruler               " line and column number of the cursor position
set wildmenu            " enhanced command completion
set visualbell          " use visual bell instead of beeping
set laststatus=2        " always show the status lines
set list listchars=tab:→\ ,trail:▸
set cursorline

" highlight spell errors
hi SpellErrors guibg=red guifg=black ctermbg=red ctermfg=black
" toggle spell check with F7
map <F7> :setlocal spell! spell?<CR>

" behavior
" ignore these files when completing names and in explorer
set wildignore=.svn,CVS,.git,.hg,*.o,*.a,*.class,*.mo,*.la,*.so,*.obj,*.swp,*.jpg,*.png,*.xpm,*.gif
set shell=$SHELL        " use current shell for shell commands
set hidden              " enable multiple modified buffers
set autoread            " automatically read feil that has been changed on disk and doesn't have changes in vim
set backspace=indent,eol,start
set guioptions-=T       " disable toolbar"
set completeopt=menuone,preview
set cinoptions=:0,(s,u0,U1,g0,t0 " some indentation options ':h cinoptions' for details
set modelines=5         " number of lines to check for vim: directives at the start/end of file
set autoindent          " automatically indent new line
set lazyredraw          " Don't redraw screen when running macros

" tab set to 2 please
set ts=2                " number of spaces in a tab
set sw=2                " number of spaces for indent
set et                  " expand tabs into spaces

" map all the things
let mapleader = ","
let maplocalleader = "\\"
imap jj <esc>
imap <S-Tab> <C-P>

" highlight spell errors
hi SpellErrors guibg=red guifg=black ctermbg=red ctermfg=black
" toggle spell check with F7
map <F7> :setlocal spell! spell?<CR>
map <Leader>p :set paste<CR><esc>"*]p:set nopaste<cr>:retab<cr>
map <Leader>t :NERDTreeToggle<CR>

" mouse settings
if has("mouse")
  set mouse=c
endif
set mousehide           " Hide mouse pointer on insert mode."

" search settings
set incsearch           " Incremental search
set hlsearch            " Highlight search match
set ignorecase          " Do case insensitive matching
set smartcase           " do not ignore if search pattern has CAPS

" directory settings
set nobackup            " do not write backup files
set noswapfile          " do not write .swp files
silent !mkdir -vp ~/.backup/vim/undo/ > /dev/null 2>&1
set backupdir=~/.backup/vim,.       " list of directories for the backup file
set directory=~/.backup/vim,~/tmp,. " list of directory names for the swap file
set undofile
set undodir=~/.backup/vim/undo/,~/tmp,.
set autochdir            " change to the dir of the file

" folding
set foldcolumn=0        " columns for folding
set foldmethod=indent
set foldlevel=9
set nofoldenable        "dont fold by default "

" Remember last position in file
autocmd BufReadPost *
  \ if line("'\"") > 0 && line("'\"") <= line("$") |
  \   exe "normal g`\"" |
  \ endif

setlocal fo+=aw
set history=10000
let g:airline_theme="cobalt2"
syntax on
" removing trailing spaces
autocmd BufWritePre *.* %s/\s\+$//e
